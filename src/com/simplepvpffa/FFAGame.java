package com.simplepvpffa;

import com.simplepvp.Game;
import com.simplepvp.util.SimpleUtil;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.HashMap;

public class FFAGame extends Game {

    // The variables needed for the game.
    private final HashMap<Player, Integer> playerLives = new HashMap<>();

    // Initialise the game.
    public FFAGame(String gameName, Material gameIcon, FFAMapConfig mapConfig) {
        super(gameName, gameIcon, mapConfig); // Call the super.
    }

/*----------------------------------------------------------------------------------------------------------------------
                                                    Setup Methods
----------------------------------------------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------------------------------------------
                                                  Base Methods
----------------------------------------------------------------------------------------------------------------------*/

    @Override
    public boolean sendPlayer(Player player) {

        if (super.sendPlayer(player)) {
            playerLives.put(player, SimplePVPFFA.getPlayerLives());
            player.getInventory().addItem(SimplePVPFFA.getKits().getKitMenuItem());
            return true;
        }
        return false;
    }

    @Override
    public boolean removePlayer(Player player) {
        // Only run if the player was removed from the game.
        if (super.removePlayer(player)) {
            playerLives.remove(player);
            checkGameWin();
            return true;
        }
        return false;
    }

    @Override
    protected void killPlayer(Player player, Player damager, EntityDamageEvent.DamageCause cause) {
        super.killPlayer(player, damager, cause);

        int lastLifeCount = playerLives.get(player);
        playerLives.replace(player, lastLifeCount - 1);

        if (lastLifeCount > 3) {
            player.sendMessage(ChatColor.RED + "You now have only " + (lastLifeCount - 1) + " lives left.");
        } else if (lastLifeCount == 3) {
            player.sendMessage(ChatColor.RED + "You now have only 1 life left!");
        } else if (lastLifeCount == 2) {
            player.sendMessage(ChatColor.RED + "You have no lives left! Use this one well.");
        }

        if (lastLifeCount > 1) {
            player.sendTitle(ChatColor.RED + "You Died!", "You will respawn soon.", 0, 20, 10);
            respawnPlayer(player);
        } else {
            player.sendTitle(ChatColor.RED + "You Died!", "Game over for you.", 0, 20, 10);
        }

        checkGameWin();
    }

    @Override
    protected void respawnPlayer(Player player) {
        Bukkit.getScheduler().runTaskLater(SimplePVPFFA.getPlugin(), () -> {
            if (gameStarted && !gameEnded) {
                player.teleport(((FFAMapConfig) getMapConfig()).getPvpSpawnLocation());
                player.setGameMode(GameMode.SURVIVAL);
                SimplePVPFFA.getKits().equipKit(player);
            }
        }, 60);
    }

    @Override
    protected void startGame() {
        // Teleport all the players and give them their items.
        for (Player player : getPlayers()) {
            player.teleport(((FFAMapConfig) getMapConfig()).getPvpSpawnLocation());
            player.setGameMode(GameMode.SURVIVAL);
            SimplePVPFFA.getKits().equipKit(player);
        }
    }

    @Override
    protected void checkGameWin() {
        if (gameStarted && !gameEnded) {
            int alivePlayerCount = 0;
            Player lastAlivePlayer = null;

            for (Player player : playerLives.keySet()) {
                if (playerLives.get(player) > 0) {
                    alivePlayerCount++;
                    lastAlivePlayer = player;
                }

                if (alivePlayerCount > 1) {
                    break;
                }
            }

            if (alivePlayerCount <= 1) {
                endGame(lastAlivePlayer);
            }
        }
    }

    // End the game.
    private void endGame(Player player) {
        // Show the winner if there is one.
        if (player != null) {
            SimpleUtil.sendPlayersMessage(getPlayers(),
                    ChatColor.YELLOW + player.getName() + ChatColor.GOLD + " has won the game!");
            player.sendTitle(ChatColor.BLUE + "You Win!", "", 0, 20, 10);
        } else {
            // Could happen but shouldn't
            SimpleUtil.sendPlayersMessage(getPlayers(), ChatColor.GOLD + "Nobody won the game!");
        }

        super.endGame();
    }

/*----------------------------------------------------------------------------------------------------------------------
                                                    Game Methods
----------------------------------------------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------------------------------------------
                                                    Game Events
----------------------------------------------------------------------------------------------------------------------*/

}