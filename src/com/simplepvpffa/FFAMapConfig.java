package com.simplepvpffa;

import com.simplepvp.MapConfig;
import com.simplepvp.util.SimpleRegion;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public class FFAMapConfig extends MapConfig {

    private final List<SimpleRegion> pvpSpawnRegions = new ArrayList<>();
    private final Location defaultPVPSpawn;
    private final Random random = new Random(System.currentTimeMillis());

    public FFAMapConfig(ConfigurationSection cs) {
        // Call the super.
        super(convertToTitleCase(cs.getName().replace('-', ' ')),
                cs.getInt("min-players"), cs.getInt("max-players"), cs.getLocation("spawn-location"));

        defaultPVPSpawn = cs.getLocation("default-pvp-spawn-location");

        ConfigurationSection pvpSpawnRegionSections = cs.getConfigurationSection("pvp-spawn-regions");
        if (pvpSpawnRegionSections != null) {
            for (String key : pvpSpawnRegionSections.getKeys(false)){
                pvpSpawnRegions.add(new SimpleRegion(
                        pvpSpawnRegionSections.getInt(key + ".first-corner.x"),
                        pvpSpawnRegionSections.getInt(key + ".first-corner.y"),
                        pvpSpawnRegionSections.getInt(key + ".first-corner.z"),
                        pvpSpawnRegionSections.getInt(key + ".second-corner.x"),
                        pvpSpawnRegionSections.getInt(key + ".second-corner.y"),
                        pvpSpawnRegionSections.getInt(key + ".second-corner.z")
                ));
            }
        }

    }

    // Used to convert strings to title case. Used for map names.
    public static String convertToTitleCase(String text) {
        StringBuilder converted = new StringBuilder();

        boolean upperCaseNext = true;
        for (char c : text.toCharArray()) {
            if (Character.isSpaceChar(c)) {
                upperCaseNext  = true;
            } else if (upperCaseNext) {
                c = Character.toUpperCase(c);
                upperCaseNext  = false;
            } else {
                c = Character.toLowerCase(c);
            }
            converted.append(c);
        }

        return converted.toString();
    }

    // Getters.
    public Location getPvpSpawnLocation() {
        for (int i = 0; i < 5; i++) {
            SimpleRegion selectedRegion = pvpSpawnRegions.get(random.nextInt(pvpSpawnRegions.size()));
            int selX = selectedRegion.getPos1().x() + random.nextInt(selectedRegion.getPos2().x() - selectedRegion.getPos1().x() + 1);
            int selZ = selectedRegion.getPos1().z() + random.nextInt(selectedRegion.getPos2().z() - selectedRegion.getPos1().z() + 1);

            for (int y = selectedRegion.getPos1().y(); y <= selectedRegion.getPos2().y(); y++) {

                Block spawnBlock = Objects.requireNonNull(getSpawnLocation().getWorld()).getBlockAt(selX, y, selZ);
                Block relUp = spawnBlock.getRelative(BlockFace.UP);
                Block relDown = spawnBlock.getRelative(BlockFace.DOWN);

                if (relDown.getType().isSolid() && spawnBlock.getType().isAir() && relUp.getType().isAir()) {
                    return spawnBlock.getLocation().add(0.5, 0, 0.5);
                }
            }

        }
        return defaultPVPSpawn;
    }
}
