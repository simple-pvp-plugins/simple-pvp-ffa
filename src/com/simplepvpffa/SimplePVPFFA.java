package com.simplepvpffa;

import com.simplepvp.Game;
import com.simplepvp.SimplePVP;
import com.simplepvp.kits.Kits;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class SimplePVPFFA extends JavaPlugin {


    private final Set<Game> gameSet = new HashSet<>(); // Store a list of games.
    private static Plugin plugin; // This plugin.
    private static Kits kits; // The kits for the games
    private static int playerLives; // How many lives the player has
    private Set<String> mapKeyList; // A list of the config map keys used for loading maps.
    private ConfigurationSection mapsConfigSection; // The configuration section that holds the maps.
    private Material gameIcon; // The icon used for the game in the game menu.

    @Override
    public void onEnable() {
        plugin = this; // Store a static plugin object to be used by other classes.

        // Create a kits object for this game.
        kits = new Kits(this);
        SimplePVP.getKitsManager().registerKits("FFA", kits);

        getConfigSections(); // Get the map config sections.
        // Create the games from the map config sections.
        for (String mapKey : mapKeyList) {
            Game tmpGame = new FFAGame("FFA", gameIcon,
                    new FFAMapConfig(Objects.requireNonNull(mapsConfigSection.getConfigurationSection(mapKey))));
            gameSet.add(tmpGame);
            SimplePVP.registerGame(tmpGame);
        }
    }

    @Override
    public void onDisable() {
        // Remove the games when this plugin becomes disabled.
        for (Game game : gameSet) {
            SimplePVP.removeGame(game);
        }
    }

    // A way for other classes to get this plugin object
    public static Plugin getPlugin() {
        return plugin;
    }

    // Static getters
    public static Kits getKits() {
        return kits;
    }

    public static int getPlayerLives() {
        return playerLives;
    }

    // Get the map configuration sections.
    private void getConfigSections() {
        saveDefaultConfig(); // Make sure the config is saved.
        FileConfiguration config = getConfig(); // Get the config.

        // Add defaults.
        config.addDefault("game-icon", Material.DIAMOND_SWORD.toString());
        config.addDefault("player-lives", 3);

        // Save defaults.
        config.options().copyDefaults(true);
        saveConfig();

        // Save the maps configuration section
        mapsConfigSection = config.getConfigurationSection("maps");

        // If the maps config section was not found, create it.
        if (mapsConfigSection == null) {
            config.createSection("maps");
            saveConfig();
            mapsConfigSection = config.getConfigurationSection("maps");
        }

        assert mapsConfigSection != null; // Throw error if still null for some reason.
        mapKeyList = mapsConfigSection.getKeys(false); // Get the map keys.

        // Get the game icon
        if (config.getString("game-icon") != null) {
            gameIcon = Material.getMaterial(config.getString("game-icon"));
            if (gameIcon == null) {
                Bukkit.getLogger().warning("Invalid Material " + config.getString("game-icon") + " in config!");
                config.set("game-icon", Objects.requireNonNull(config.getDefaults()).getString("game-icon"));
                saveConfig();
                gameIcon = Material.getMaterial(config.getString("game-icon"));
            }
        } else {
            config.set("game-icon", Objects.requireNonNull(config.getDefaults()).getString("game-icon"));
            saveConfig();
            gameIcon = Material.getMaterial(config.getString("game-icon"));
        }

        // Get game config options
        playerLives = config.getInt("player-lives");
    }

}
